package com.example.diplomaproject.di

import com.example.data.RetrofitCreator
import com.example.data.converters.CreditCardDtoConverter
import com.example.data.repository.AuthRepositoryImpl
import com.example.data.repository.NetworkRepositoryImpl
import com.example.data.repository.UserProfileRepositoryImpl
import com.example.data.storage.auth.AuthStorage
import com.example.data.storage.auth.AuthStorageImpl
import com.example.data.storage.network.NetworkService
import com.example.data.storage.network.NetworkStorage
import com.example.data.storage.network.NetworkStorageImpl
import com.example.data.storage.profile.ProfileStorage
import com.example.data.storage.profile.ProfileStoragePrefImpl
import com.example.domain.repository.AuthRepository
import com.example.domain.repository.NetworkRepository
import com.example.domain.repository.UserProfileRepository
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module

val dataModule = module {

    single { GsonBuilder().serializeNulls().create() }

    val creator = RetrofitCreator()

    single { creator.createService(get(), NetworkService::class.java) as NetworkService }

    single<AuthStorage> {
        AuthStorageImpl(get())
    }

    single<ProfileStorage> {
        ProfileStoragePrefImpl(get())
    }

    single<NetworkStorage> { NetworkStorageImpl(get()) }

    single<AuthRepository> {
        AuthRepositoryImpl(get())
    }

    single<UserProfileRepository> {
        UserProfileRepositoryImpl(get())
    }

    single<NetworkRepository> { NetworkRepositoryImpl(get(), get()) }

    factory { CreditCardDtoConverter() }
}