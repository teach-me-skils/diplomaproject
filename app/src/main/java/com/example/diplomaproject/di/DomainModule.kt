package com.example.diplomaproject.di

import com.example.domain.UseCase.*
import org.koin.dsl.module

val domainModule = module {

    factory<EditEmailUseCase> { EditEmailUseCase(get()) }

    factory<EditNameUseCase> { EditNameUseCase(get()) }

    factory<EditNumberUseCase> { EditNumberUseCase(get()) }

    factory<GetUserInformationUseCase> { GetUserInformationUseCase(get()) }

    factory<LoginUseCase> { LoginUseCase(get()) }

    factory<RegistrationUseCase> { RegistrationUseCase(get()) }

    factory<GetCreditCardUseCase> { GetCreditCardUseCase(get()) }
}