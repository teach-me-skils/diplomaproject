package com.example.diplomaproject.di

import com.example.diplomaproject.fragments.creditCard.CreditCardViewModel
import com.example.diplomaproject.fragments.login.LoginViewModel
import com.example.diplomaproject.fragments.mainWindow.profile.email.ProfileEmailViewModel
import com.example.diplomaproject.fragments.mainWindow.profile.name.ProfileNameViewModel
import com.example.diplomaproject.fragments.mainWindow.profile.number.ProfileNumberViewModel
import com.example.diplomaproject.fragments.mainWindow.profile.profile.ProfileViewModel
import com.example.diplomaproject.fragments.registration.RegistrationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<RegistrationViewModel> { RegistrationViewModel(get()) }

    viewModel<ProfileViewModel> { ProfileViewModel(get()) }

    viewModel<ProfileNumberViewModel> { ProfileNumberViewModel(get()) }

    viewModel<ProfileNameViewModel> { ProfileNameViewModel(get()) }

    viewModel<ProfileEmailViewModel> { ProfileEmailViewModel(get()) }

    viewModel<LoginViewModel> { LoginViewModel(get()) }

    viewModel<CreditCardViewModel> { CreditCardViewModel(get()) }
}

