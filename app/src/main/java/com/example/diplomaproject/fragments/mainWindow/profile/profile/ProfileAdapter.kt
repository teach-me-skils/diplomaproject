package com.example.diplomaproject.fragments.mainWindow.profile.profile

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.diplomaproject.databinding.ProfileDividerBinding
import com.example.diplomaproject.databinding.ProfileItemBinding
import com.example.diplomaproject.fragments.base.BaseViewHolder
import com.example.domain.model.ProfileDivider
import com.example.domain.model.ProfileItem

class ProfileAdapter : ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(object : DiffUtil.ItemCallback<Any>() {
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
        oldItem is ProfileItem && newItem is ProfileItem -> oldItem.title == newItem.title
        oldItem is ProfileDivider && newItem is ProfileDivider -> true
        else -> false
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
        oldItem is ProfileItem && newItem is ProfileItem -> oldItem == newItem
        oldItem is ProfileDivider && newItem is ProfileDivider -> true
        else -> false
    }


}) {

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is ProfileItem -> PROFILE_ITEM_TYPE
        is ProfileDivider -> PROFILE_DIVIDER
        else -> throw IllegalArgumentException("ProfileAdapter can't handle item" + getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        PROFILE_ITEM_TYPE -> ProfileViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        PROFILE_DIVIDER -> ProfileDividerViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("ProfileAdapter can't handle $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) {
        holder.handleItem(getItem(position))
    }

    companion object {
        private const val PROFILE_ITEM_TYPE = 999
        private const val PROFILE_DIVIDER = 998
    }
}

private class ProfileViewHolder(parent: ViewGroup) : BaseViewHolder<ProfileItemBinding, ProfileItem>(
    ProfileItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {
    override fun ProfileItemBinding.bind(value: ProfileItem) {
        title.text = value.title
        subTitle.text = value.subtitle
        itemView.setOnClickListener {
            value.action()
        }
    }
}

private class ProfileDividerViewHolder(parent: ViewGroup) : BaseViewHolder<ProfileDividerBinding, ProfileDivider>(
    ProfileDividerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
)