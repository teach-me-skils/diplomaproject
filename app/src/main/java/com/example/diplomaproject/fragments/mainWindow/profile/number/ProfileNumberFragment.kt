package com.example.diplomaproject.fragments.mainWindow.profile.number

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentProfileNumberBinding
import com.example.diplomaproject.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileNumberFragment : BaseFragment<FragmentProfileNumberBinding>() {

    private val viewModel by viewModel<ProfileNumberViewModel>()

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?
    ): FragmentProfileNumberBinding =
            FragmentProfileNumberBinding.inflate(inflater, container, false)

    override fun FragmentProfileNumberBinding.onBindView(saveInstanceState: Bundle?) {
        submit.setOnClickListener {
            viewModel.editNumber(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}