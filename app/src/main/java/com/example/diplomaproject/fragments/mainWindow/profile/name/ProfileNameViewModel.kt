package com.example.diplomaproject.fragments.mainWindow.profile.name

import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.EditNameUseCase

class ProfileNameViewModel(private val editNameUseCase: EditNameUseCase) : ViewModel()
{
    fun editName(name: String)
    {
        editNameUseCase.execute(name)
    }
}