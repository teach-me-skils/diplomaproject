package com.example.diplomaproject.fragments.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.navArgs
import com.example.diplomaproject.databinding.FragmentRegistrationBinding
import com.example.diplomaproject.fragments.base.BaseDialogFragment
import com.example.domain.model.RegistrationUserModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class RegistrationDialog: BaseDialogFragment<FragmentRegistrationBinding>() {

    private val args:RegistrationDialogArgs by navArgs()
    private val viewModel by viewModel<RegistrationViewModel>()

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?
    ): FragmentRegistrationBinding = FragmentRegistrationBinding.inflate(inflater ,container ,false)

    override fun FragmentRegistrationBinding.onBindView(saveInstanceState: Bundle?)
    {
        submit.setOnClickListener {
            val registrationUserModel = RegistrationUserModel(
                    login = addName.text.toString() ,
                    password = enterPassword.text.toString() ,
                    repeatPassword = repeatPassword.text.toString()
            )
            viewModel.registration(registrationUserModel)
        }
        viewModel.registrationLiveData.observe(viewLifecycleOwner) { result ->
            if (result)
            {
                closeWithResult(true)
            } else
            {
                Toast.makeText(
                        requireContext() ,
                        "Enter registration params" ,
                        Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun closeWithResult(isSuccess: Boolean){
        navController.popBackStack()
        setResult(isSuccess)
    }

    private fun setResult(isSuccess: Boolean){
        setFragmentResultWithRequester(args.requester, RESULT_KEY to isSuccess)
    }

    companion object{
        const val REQUEST_KEY = "REGISTRATION_DIALOG_REQUEST_KEY"
        const val RESULT_KEY = "REGISTRATION_DIALOG_RESULT_KEY"
    }
}