package com.example.diplomaproject.fragments.creditCard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentCreditCardBinding
import com.example.diplomaproject.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class CreditCardFragment : BaseFragment<FragmentCreditCardBinding>() {

    private val viewModel by viewModel<CreditCardViewModel>()

    private val adapter = CreditCardAdapter()

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?): FragmentCreditCardBinding = FragmentCreditCardBinding.inflate(inflater, container, false
    )

    override fun FragmentCreditCardBinding.onBindView(saveInstanceState: Bundle?) {
        back.setOnClickListener {
            navController.popBackStack()
        }

        items.adapter = adapter
        viewModel.getCreditCard(5)
        viewModel.creditCardLiveData.observe(viewLifecycleOwner) { creditCard ->
            adapter.submitList(creditCard)
        }
    }
}