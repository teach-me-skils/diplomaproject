package com.example.diplomaproject.fragments.mainWindow.profile.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentProfileBinding
import com.example.diplomaproject.fragments.base.BaseFragment
import com.example.domain.model.ProfileDivider
import com.example.domain.model.ProfileItem
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding>() {

    private val viewModel by viewModel<ProfileViewModel>()

    private val adapter = ProfileAdapter()

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?): FragmentProfileBinding = FragmentProfileBinding.inflate(inflater, container, false
    )

    override fun FragmentProfileBinding.onBindView(saveInstanceState: Bundle?) {
        items.adapter = adapter
        viewModel.getUserInfo()
        viewModel.userInfoLiveData.observe(viewLifecycleOwner) { userProfile ->
            adapter.submitList(
                    listOf(ProfileItem("Номер", userProfile.number) {
                        navController.navigate(ProfileFragmentDirections.navigateToProfileNumberFragment())
                    },
                            ProfileDivider,
                            ProfileItem("Имя", userProfile.name) {
                                navController.navigate(ProfileFragmentDirections.navigateToProfileNameFragment())
                            },
                            ProfileDivider,
                            ProfileItem("email", userProfile.email) {
                                navController.navigate(ProfileFragmentDirections.navigateToProfileEmailFragment())
                            },
                            ProfileDivider,
                            ProfileItem("Мои карты", "") {
                                navController.navigate(ProfileFragmentDirections.navigateToCreditCardFragment())
                            },
                            ProfileDivider
                    )
            )
        }
        search.setOnClickListener {
            navController.navigate(ProfileFragmentDirections.navigateToMainFragment())
        }
        travel.setOnClickListener {
            navController.navigate(ProfileFragmentDirections.navigateToWindowTravelFragment())
        }
        help.setOnClickListener {
            navController.navigate(ProfileFragmentDirections.navigateToWindowHelpFragment())
        }
    }
}
