package com.example.diplomaproject.fragments.mainWindow.profile.email

import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.EditEmailUseCase

class ProfileEmailViewModel(private val editEmailUseCase: EditEmailUseCase) : ViewModel()
{
    fun editEmail(email: String)
    {
        editEmailUseCase.execute(email)
    }
}