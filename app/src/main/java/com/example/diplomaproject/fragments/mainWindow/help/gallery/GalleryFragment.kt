package com.example.diplomaproject.fragments.mainWindow.help.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.diplomaproject.databinding.FragmentGalleryBinding
import com.example.diplomaproject.fragments.base.BaseFragment

class GalleryFragment() : BaseFragment<FragmentGalleryBinding>() {
    override fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?):
            FragmentGalleryBinding = FragmentGalleryBinding.inflate(inflater, container, false)

    override fun FragmentGalleryBinding.onBindView(saveInstanceState: Bundle?) {
        Glide.with(this@GalleryFragment)
                .load("https://minsk-lida.by/img/slider/image8.jpg")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerInside()
                .into(image1)

        Glide.with(this@GalleryFragment)
                .load("https://minsk-lida.by/img/slider/image10.jpg")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerInside()
                .into(image2)
    }
}

