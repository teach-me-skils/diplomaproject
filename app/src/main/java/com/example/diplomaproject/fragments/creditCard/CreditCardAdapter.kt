package com.example.diplomaproject.fragments.creditCard

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.diplomaproject.databinding.CreditCardItemBinding
import com.example.diplomaproject.fragments.base.BaseViewHolder
import com.example.domain.model.CreditCardDto

class CreditCardAdapter : ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(object : DiffUtil.ItemCallback<Any>() {
    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
        oldItem is CreditCardDto && newItem is CreditCardDto -> oldItem.type == oldItem.type
        else -> false
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
            oldItem: Any,
            newItem: Any): Boolean = when {
        oldItem is CreditCardDto && newItem is CreditCardDto -> oldItem == newItem
        else -> false
    }

}) {

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is CreditCardDto -> CreditCardAdapter.CREDIT_CARD_DTO
        else -> throw IllegalArgumentException("CreditCardAdapter can't handle item" + getItem(position))
    }

    override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        CREDIT_CARD_DTO -> FragmentCreditCardViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("CreditCardAdapter can't handle $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) {
        holder.handleItem(getItem(position))
    }

    companion object {
        private const val CREDIT_CARD_DTO = 997
    }
}

private class FragmentCreditCardViewHolder(private val parent: ViewGroup) : BaseViewHolder<CreditCardItemBinding, CreditCardDto>(
        CreditCardItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
) {
    override fun CreditCardItemBinding.bind(value: CreditCardDto) {
        type.text = value.type
        number.text = value.number
        expiration.text = value.expiration
        owner.text = value.owner
    }
}