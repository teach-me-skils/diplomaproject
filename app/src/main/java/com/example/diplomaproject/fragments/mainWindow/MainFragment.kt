package com.example.diplomaproject.fragments.mainWindow

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentMainBinding
import com.example.diplomaproject.fragments.base.BaseFragment

class MainFragment : BaseFragment<FragmentMainBinding>() {

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?
    ): FragmentMainBinding = FragmentMainBinding.inflate(inflater, container, false)

    override fun FragmentMainBinding.onBindView(saveInstanceState: Bundle?) {
        help.setOnClickListener {
            navController.navigate(MainFragmentDirections.navigateToWindowHelpFragment())
        }
        travel.setOnClickListener {
            navController.navigate(MainFragmentDirections.navigateToWindowTravelFragment())
        }
        profile.setOnClickListener {
            navController.navigate(MainFragmentDirections.navigateToProfileFragment())
        }
    }
}