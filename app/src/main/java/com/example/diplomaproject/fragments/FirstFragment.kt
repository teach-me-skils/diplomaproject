package com.example.diplomaproject.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentFirstBinding
import com.example.diplomaproject.fragments.base.BaseFragment
import com.example.diplomaproject.fragments.registration.RegistrationDialog

class FirstFragment : BaseFragment<FragmentFirstBinding>() {

    override fun createViewBinding(
            inflater: LayoutInflater,
            container: ViewGroup?
    ): FragmentFirstBinding = FragmentFirstBinding.inflate(inflater, container, false)

    override fun FragmentFirstBinding.onBindView(saveInstanceState: Bundle?) {
        login.setOnClickListener {
            navController.navigate(FirstFragmentDirections.navigateToLoginFragment())
        }
        registration.setOnClickListener {
            val requester = setResultListenerForWithRegistrationDialog()
            navController.navigate(FirstFragmentDirections.navigateToRegistrationDialog(requester))
        }
    }

    private fun setResultListenerForWithRegistrationDialog() =
            setResultListenerWithCheckRequester(RegistrationDialog.REQUEST_KEY) {
                it.getBoolean(RegistrationDialog.RESULT_KEY)
            }
}