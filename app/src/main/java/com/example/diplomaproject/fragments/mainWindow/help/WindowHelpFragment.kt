package com.example.diplomaproject.fragments.mainWindow.help

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.example.diplomaproject.databinding.FragmentHelpBinding
import com.example.diplomaproject.fragments.base.BaseFragment

class WindowHelpFragment : BaseFragment<FragmentHelpBinding>() {
    override fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?):
            FragmentHelpBinding = FragmentHelpBinding.inflate(inflater, container, false)

    override fun FragmentHelpBinding.onBindView(saveInstanceState: Bundle?) {
        search.setOnClickListener {
            navController.navigate(WindowHelpFragmentDirections.navigateToMainFragment())
        }
        travel.setOnClickListener {
            navController.navigate(WindowHelpFragmentDirections.navigateToWindowTravelFragment())
        }
        profile.setOnClickListener {
            navController.navigate(WindowHelpFragmentDirections.navigateToProfileFragment())
        }
        gallery.setOnClickListener {
            navController.navigate(WindowHelpFragmentDirections.navigateToGalleryFragment())
        }
        contacts.setOnClickListener {
            Toast.makeText(
                    requireContext(),
                    "разработка",
                    Toast.LENGTH_SHORT
            ).show()
        }
        termsOfUse.setOnClickListener {
            Toast.makeText(
                    requireContext(),
                    "разработка",
                    Toast.LENGTH_SHORT
            ).show()
        }
        question.setOnClickListener {
            Toast.makeText(
                    requireContext(),
                    "разработка",
                    Toast.LENGTH_SHORT
            ).show()
        }
    }
}