package com.example.diplomaproject.fragments.mainWindow.profile.profile

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.GetUserInformationUseCase
import com.example.domain.model.UserProfileInfo

class ProfileViewModel(private val getUserInformationUseCase: GetUserInformationUseCase) :
    ViewModel()
{

    private val _userInfoLiveData = MutableLiveData<UserProfileInfo>()
    val userInfoLiveData: LiveData<UserProfileInfo> = _userInfoLiveData

    fun getUserInfo()
    {
        val result = getUserInformationUseCase.execute()
        _userInfoLiveData.value = result
    }
}