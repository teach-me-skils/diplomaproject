package com.example.diplomaproject.fragments.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.UseCase.LoginUseCase
import com.example.domain.model.UserCredo

class LoginViewModel(private val loginUseCase: LoginUseCase) : ViewModel()
{

    private val _loginLiveData = MutableLiveData<Boolean>()
    val loginLiveData: LiveData<Boolean> = _loginLiveData

    fun login(model: UserCredo)
    {
        val result = loginUseCase.execute(param = model)
        _loginLiveData.value = result
    }
}