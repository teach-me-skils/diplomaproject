package com.example.diplomaproject.fragments.mainWindow.travel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.diplomaproject.databinding.FragmentTravelBinding
import com.example.diplomaproject.fragments.base.BaseFragment

class WindowTravelFragment : BaseFragment<FragmentTravelBinding>() {
    override fun createViewBinding(inflater: LayoutInflater, container: ViewGroup?):
            FragmentTravelBinding = FragmentTravelBinding.inflate(inflater, container, false)

    override fun FragmentTravelBinding.onBindView(saveInstanceState: Bundle?) {
        search.setOnClickListener {
            navController.navigate(WindowTravelFragmentDirections.navigateToMainFragment())
        }
        help.setOnClickListener {
            navController.navigate(WindowTravelFragmentDirections.navigateToWindowHelpFragment())
        }
        profile.setOnClickListener {
            navController.navigate(WindowTravelFragmentDirections.navigateToProfileFragment())
        }
    }
}