package com.example.diplomaproject.application

import android.app.Application
import com.example.diplomaproject.di.appModule
import com.example.diplomaproject.di.dataModule
import com.example.diplomaproject.di.domainModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App : Application()
{
    override fun onCreate()
    {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(listOf(appModule ,dataModule ,domainModule))
        }
    }
}