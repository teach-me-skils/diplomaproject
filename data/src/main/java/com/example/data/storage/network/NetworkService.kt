package com.example.data.storage.network


import com.example.data.*
import com.example.data.storage.network.model.CreditCard
import retrofit2.Response

import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkService {

    @GET(CREDIT_CARD)
    suspend fun getCreditCard(
        @Query(QUANTITY) quantity: Int ,
        @Query(LOCALE) locale: String
    ): Response<BaseResponse<ArrayList<CreditCard>>>

}