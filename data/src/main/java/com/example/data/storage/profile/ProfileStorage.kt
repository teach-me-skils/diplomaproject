package com.example.data.storage.profile

import com.example.domain.model.UserProfileInfo

interface ProfileStorage
{
    fun getProfileInfo():UserProfileInfo

    fun editPhoneNumber(number: String)

    fun editUserName(name: String)

    fun editUserEmail(email: String)
}