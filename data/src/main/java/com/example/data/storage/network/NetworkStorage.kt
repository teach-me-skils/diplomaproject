package com.example.data.storage.network

import com.example.data.BaseResponse
import com.example.data.storage.network.model.CreditCard
import retrofit2.Response

interface NetworkStorage {

    suspend fun getCreditCard(count: Int): Response<BaseResponse<ArrayList<CreditCard>>>
}