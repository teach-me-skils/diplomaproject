package com.example.data.storage.auth

import com.example.domain.model.UserCredo

interface AuthStorage {

    fun saveCredo(userCredo: UserCredo)

    fun getCredo(): UserCredo
}