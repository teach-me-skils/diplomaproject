package com.example.data.storage.network.model

import com.google.gson.annotations.SerializedName

data class CreditCard(
    @SerializedName("type") var type: String? = null ,
    @SerializedName("number") var number: String? = null ,
    @SerializedName("expiration") var expiration: String? = null ,
    @SerializedName("owner") var owner: String? = null
)
