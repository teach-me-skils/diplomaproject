package com.example.data.repository

import com.example.data.storage.profile.ProfileStorage
import com.example.domain.model.UserProfileInfo
import com.example.domain.repository.UserProfileRepository

class UserProfileRepositoryImpl(private val profileStorage: ProfileStorage) : UserProfileRepository
{
    override fun getUserProfileInfo(): UserProfileInfo
    {
        return profileStorage.getProfileInfo()
    }

    override fun editProfileNumber(number: String)
    {
        return profileStorage.editPhoneNumber(number)
    }

    override fun editProfileName(name: String)
    {
       return profileStorage.editUserName(name)
    }

    override fun editProfileEmail(email: String)
    {
       return profileStorage.editUserEmail(email)
    }


}