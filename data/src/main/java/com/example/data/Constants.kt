package com.example.data

const val BASE_URL = "https://fakerapi.it/api/v1/"

const val CREDIT_CARD = "credit_cards"

const val QUANTITY = "_quantity"
const val LOCALE = "_locale"