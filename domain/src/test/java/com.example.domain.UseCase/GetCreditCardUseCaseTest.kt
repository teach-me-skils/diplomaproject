package com.example.domain.UseCase

import com.example.domain.model.CreditCardDto
import com.example.domain.repository.NetworkRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.mock

class GetCreditCardUseCaseTest {

    private companion object {
        const val TYPE_TEST = "type"
        const val NUMBER_TEST = "number"
        const val EXPIRATION_TEST = "expiration"
        const val OWNER_TEST = "owner"

        const val COUNT_TEST = 1
    }

    private val fakeNetworkRepository = mock<NetworkRepository>()

    @AfterEach
    fun tearDown() {
        Mockito.reset(fakeNetworkRepository)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `should return the same list of credit card as in the repository`() = runTest {
        val testList = listOf(
                CreditCardDto(
                        TYPE_TEST,
                        NUMBER_TEST,
                        EXPIRATION_TEST,
                        OWNER_TEST
                )
        )

        Mockito.`when`(fakeNetworkRepository.getCreditCard(COUNT_TEST)).thenReturn(testList)

        val useCase = GetCreditCardUseCase(fakeNetworkRepository)
        val actualResult = useCase.execute(COUNT_TEST)

        val expectedResult = listOf(
                CreditCardDto(
                        TYPE_TEST,
                        NUMBER_TEST,
                        EXPIRATION_TEST,
                        OWNER_TEST
                )
        )
        Assertions.assertEquals(expectedResult, actualResult)
    }
}