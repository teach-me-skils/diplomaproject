package com.example.domain.repository

import com.example.domain.model.CreditCardDto

interface NetworkRepository {

    suspend fun getCreditCard(count: Int): List<CreditCardDto>
}