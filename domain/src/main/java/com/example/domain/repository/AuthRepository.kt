package com.example.domain.repository

import com.example.domain.model.RegistrationUserModel
import com.example.domain.model.UserCredo

interface AuthRepository {

    fun checkUserCredo(userCredo: UserCredo): Boolean

    fun registerUser(model: RegistrationUserModel): Boolean
}