package com.example.domain.UseCase

import com.example.domain.base.SuspendUseCase
import com.example.domain.model.CreditCardDto
import com.example.domain.repository.NetworkRepository

class   GetCreditCardUseCase(private val networkRepository: NetworkRepository) :
        SuspendUseCase<Int, List<CreditCardDto>> {
    override suspend fun execute(param: Int?): List<CreditCardDto> {
        return networkRepository.getCreditCard(param !!)
    }
}