package com.example.domain.model

data class UserCredo(
    val login: String,
    val password: String
)