package com.example.domain.model

data class RegistrationUserModel(
    val login: String,
    val password: String,
    val repeatPassword: String
)