package com.example.domain.model

data class UserProfileInfo(
    val number: String,
    val name: String,
    val email: String,
)